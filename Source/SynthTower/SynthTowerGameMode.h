// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SynthTowerGameMode.generated.h"

UCLASS(minimalapi)
class ASynthTowerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASynthTowerGameMode();
};




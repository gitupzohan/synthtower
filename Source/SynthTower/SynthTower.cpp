// Copyright Epic Games, Inc. All Rights Reserved.

#include "SynthTower.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SynthTower, "SynthTower" );
 
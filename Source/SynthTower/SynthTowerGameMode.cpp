// Copyright Epic Games, Inc. All Rights Reserved.

#include "SynthTowerGameMode.h"
#include "SynthTowerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "SynthTowerCharacter.h"

ASynthTowerGameMode::ASynthTowerGameMode()
{
	// set default pawn class to our Blueprinted character
/*
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
	DefaultPawnClass = ASynthTowerCharacter::StaticClass();
}
